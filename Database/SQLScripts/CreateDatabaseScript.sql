if not exists(select 1 from sys.databases s where s.name = 'TeachersDB')
	create database[TeachersDB]
go

use[TeachersDB]
go

if not exists(select 1 from sys.objects s where s.name = 'Position' and s.type = 'U')
	create table [dbo].[Position]
	(
		[Id] int identity(1,1) not null
		,[Name] nvarchar(50) not null
		,[RateHour] float not null
		,constraint [pk_Position] primary key ([Id])
		,constraint [uq_Position_Name] unique ([Name]) 
	)
go

if not exists(select 1 from sys.objects s where s.name = 'Subject' and s.type = 'U')
	create table [dbo].[Subject]
	(
		[Id] int identity(1,1) not null
		,[Name] nvarchar(120) not null
		,constraint [pk_Subject] primary key([Id])
		,constraint [uq_Subject_Name] unique ([Name]) 
	)
go

if not exists(select 1 from sys.objects s where s.name = 'Teacher' and s.type = 'U')
	create table [dbo].[Teacher]
	(
		[Id] int identity(1,1) not null
		,[Surname] nvarchar(50) not null
		,[Name] nvarchar(50) not null
		,[MiddleName] nvarchar(50) not null
		,[IdPosition] int not null
		,[Mobile] nvarchar(10) not null
		,[Address] nvarchar(120) not null
	    ,constraint [pk_Teacher_Id] primary key ([Id])
		,constraint [fk_Id_Position] foreign key ([IdPosition]) references [dbo].[Position] ([Id]) on delete cascade
	)
go

if not exists(select 1 from sys.objects s where s.name = 'Load' and s.type = 'U')
	create table [dbo].[Load]
	(
		[Id] int identity(1,1) not null
		,[IdTeacher] int not null
		,[IdSubject] int not null
		,[Hour] int not null
	    ,constraint [pk_Load_Id] primary key ([Id])
		,constraint [fk_Id_Teacher] foreign key ([IdTeacher]) references [dbo].[Teacher] ([Id]) on delete cascade
		,constraint [fk_Id_Subject] foreign key ([IdSubject]) references [dbo].[Subject] ([Id]) on delete cascade
	)
go

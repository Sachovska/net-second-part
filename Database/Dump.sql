USE [TeachersDB]
GO
/****** Object:  Table [dbo].[Load]    Script Date: 08.03.2019 13:26:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Load](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdTeacher] [int] NOT NULL,
	[IdSubject] [int] NOT NULL,
	[Hour] [int] NOT NULL,
 CONSTRAINT [pk_Load_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Position]    Script Date: 08.03.2019 13:26:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Position](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[RateHour] [float] NOT NULL,
 CONSTRAINT [pk_Position] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Subject]    Script Date: 08.03.2019 13:26:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Subject](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](120) NOT NULL,
 CONSTRAINT [pk_Subject] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Teacher]    Script Date: 08.03.2019 13:26:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teacher](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[MiddleName] [nvarchar](50) NOT NULL,
	[IdPosition] [int] NOT NULL,
	[Mobile] [nvarchar](10) NOT NULL,
	[Address] [nvarchar](120) NOT NULL,
 CONSTRAINT [pk_Teacher_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Load] ON 

INSERT [dbo].[Load] ([Id], [IdTeacher], [IdSubject], [Hour]) VALUES (1, 1, 3, 90)
INSERT [dbo].[Load] ([Id], [IdTeacher], [IdSubject], [Hour]) VALUES (2, 4, 3, 80)
INSERT [dbo].[Load] ([Id], [IdTeacher], [IdSubject], [Hour]) VALUES (3, 2, 3, 90)
INSERT [dbo].[Load] ([Id], [IdTeacher], [IdSubject], [Hour]) VALUES (4, 3, 2, 120)
INSERT [dbo].[Load] ([Id], [IdTeacher], [IdSubject], [Hour]) VALUES (5, 4, 2, 50)
SET IDENTITY_INSERT [dbo].[Load] OFF
SET IDENTITY_INSERT [dbo].[Position] ON 

INSERT [dbo].[Position] ([Id], [Name], [RateHour]) VALUES (1, N'Доцент', 150)
INSERT [dbo].[Position] ([Id], [Name], [RateHour]) VALUES (2, N'Асистент', 100)
INSERT [dbo].[Position] ([Id], [Name], [RateHour]) VALUES (3, N'Професор', 200)
SET IDENTITY_INSERT [dbo].[Position] OFF
SET IDENTITY_INSERT [dbo].[Subject] ON 

INSERT [dbo].[Subject] ([Id], [Name]) VALUES (4, N'Інтелектуальні інформаційні системи')
INSERT [dbo].[Subject] ([Id], [Name]) VALUES (3, N'Платформи корпоративних інформаційних систем')
INSERT [dbo].[Subject] ([Id], [Name]) VALUES (1, N'Проектування програмних систем')
INSERT [dbo].[Subject] ([Id], [Name]) VALUES (2, N'Розробка комп''ютерних ігор')
SET IDENTITY_INSERT [dbo].[Subject] OFF
SET IDENTITY_INSERT [dbo].[Teacher] ON 

INSERT [dbo].[Teacher] ([Id], [Surname], [Name], [MiddleName], [IdPosition], [Mobile], [Address]) VALUES (1, N'Романенко', N'Наталя', N'Вікторівна', 2, N'0990234532', N'м. Чернівці')
INSERT [dbo].[Teacher] ([Id], [Surname], [Name], [MiddleName], [IdPosition], [Mobile], [Address]) VALUES (2, N'Мельник', N'Галина', N'Василівна', 1, N'0950993456', N'м. Чернівці')
INSERT [dbo].[Teacher] ([Id], [Surname], [Name], [MiddleName], [IdPosition], [Mobile], [Address]) VALUES (3, N'Дорош', N'Андрій', N'Богданович', 1, N'0445683456', N'м. Чернівці')
INSERT [dbo].[Teacher] ([Id], [Surname], [Name], [MiddleName], [IdPosition], [Mobile], [Address]) VALUES (4, N'Горбатенко', N'Микола', N'Юрійович', 1, N'0560953421', N'м. Чернівці')
SET IDENTITY_INSERT [dbo].[Teacher] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [uq_Position_Name]    Script Date: 08.03.2019 13:26:35 ******/
ALTER TABLE [dbo].[Position] ADD  CONSTRAINT [uq_Position_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [uq_Subject_Name]    Script Date: 08.03.2019 13:26:35 ******/
ALTER TABLE [dbo].[Subject] ADD  CONSTRAINT [uq_Subject_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Load]  WITH CHECK ADD  CONSTRAINT [fk_Id_Subject] FOREIGN KEY([IdSubject])
REFERENCES [dbo].[Subject] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Load] CHECK CONSTRAINT [fk_Id_Subject]
GO
ALTER TABLE [dbo].[Load]  WITH CHECK ADD  CONSTRAINT [fk_Id_Teacher] FOREIGN KEY([IdTeacher])
REFERENCES [dbo].[Teacher] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Load] CHECK CONSTRAINT [fk_Id_Teacher]
GO
ALTER TABLE [dbo].[Teacher]  WITH CHECK ADD  CONSTRAINT [fk_Id_Position] FOREIGN KEY([IdPosition])
REFERENCES [dbo].[Position] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Teacher] CHECK CONSTRAINT [fk_Id_Position]
GO

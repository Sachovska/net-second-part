﻿namespace Entities.Models
{
    public partial class Load
    {
        public int Id { get; set; }
        public int IdTeacher { get; set; }
        public int IdSubject { get; set; }
        public int Hour { get; set; }

        public virtual Subject IdSubjectNavigation { get; set; }
        public virtual Teacher IdTeacherNavigation { get; set; }
    }
}

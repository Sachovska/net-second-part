﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities.Models
{
    public partial class Position
    {
        public Position()
        {
            Teacher = new HashSet<Teacher>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public double RateHour { get; set; }

        public virtual ICollection<Teacher> Teacher { get; set; }
    }
}

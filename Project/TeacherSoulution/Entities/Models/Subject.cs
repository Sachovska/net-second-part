﻿using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Subject
    {
        public Subject()
        {
            Load = new HashSet<Load>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Load> Load { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;

namespace Entities.Models
{
    public partial class TeachersDBContext : DbContext
    {
        public TeachersDBContext()
        {
        }

        public TeachersDBContext(DbContextOptions<TeachersDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Load> Load { get; set; }
        public virtual DbSet<Position> Position { get; set; }
        public virtual DbSet<Subject> Subject { get; set; }
        public virtual DbSet<Teacher> Teacher { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-8IEERJC\\SQLEXPRESS; Database=TeachersDB; user id=Vita;password=root;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<Load>(entity =>
            {
                entity.HasOne(d => d.IdSubjectNavigation)
                    .WithMany(p => p.Load)
                    .HasForeignKey(d => d.IdSubject)
                    .HasConstraintName("fk_Id_Subject");

                entity.HasOne(d => d.IdTeacherNavigation)
                    .WithMany(p => p.Load)
                    .HasForeignKey(d => d.IdTeacher)
                    .HasConstraintName("fk_Id_Teacher");
            });

            modelBuilder.Entity<Position>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("uq_Position_Name")
                    .IsUnique();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Subject>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("uq_Subject_Name")
                    .IsUnique();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(120);
            });

            modelBuilder.Entity<Teacher>(entity =>
            {
                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(120);

                entity.Property(e => e.MiddleName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Mobile)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.IdPositionNavigation)
                    .WithMany(p => p.Teacher)
                    .HasForeignKey(d => d.IdPosition)
                    .HasConstraintName("fk_Id_Position");
            });
        }
    }
}

﻿using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Teacher
    {
        public Teacher()
        {
            Load = new HashSet<Load>();
        }

        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public int IdPosition { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }

        public virtual Position IdPositionNavigation { get; set; }
        public virtual ICollection<Load> Load { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Entities.DTOModels
{
    public class SubjectDTO
    {
        public int Id { get; set; }

        [Required]
        [StringLength(120, ErrorMessage = "The name should have length till 50 charactares")]
        public string Name { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Entities.DTOModels
{
    public class LoadDTO
    {
        public int Id { get; set; }

        [Required]
        public int IdTeacher { get; set; }

        [Required]
        public int IdSubject { get; set; }

        [Required]
        public int Hour { get; set; }

    }
}

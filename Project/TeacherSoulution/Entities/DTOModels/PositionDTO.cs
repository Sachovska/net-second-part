﻿using System.ComponentModel.DataAnnotations;

namespace Entities.DTOModels
{
    public class PositionDTO
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The name should have length till 50 charactares")]
        public string Name { get; set; }

        [Required]
        public double RateHour { get; set; }
    }
}

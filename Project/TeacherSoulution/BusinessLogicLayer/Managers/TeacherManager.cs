﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer.Interfaces;
using Entities.DTOModels;
using Entities.Models;

namespace BusinessLogicLayer.Managers
{
    public class TeacherManager : IBaseManager<TeacherDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidator _validator;

        public TeacherManager(IUnitOfWork unitOfWork, IValidator validator)
        {
            _unitOfWork = unitOfWork;
            _validator = validator;
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
        }

        public void Create(TeacherDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.TeacherRepository.Create(Mapper.Map<Teacher>(item));
            _unitOfWork.Save();
        }

        public void Update(TeacherDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.TeacherRepository.Update(Mapper.Map<Teacher>(item));
            _unitOfWork.Save();
        }

        public void Delete(int id)
        {
            ValidateId(id);
            _unitOfWork.TeacherRepository.Delete(id);
            _unitOfWork.Save();
        }

        public TeacherDTO GetById(int id)
        {
            ValidateId(id);
            return Mapper.Map<TeacherDTO>(_unitOfWork.TeacherRepository.GetById(id));
        }

        public IEnumerable<TeacherDTO> GetAll()
        {
            return _unitOfWork.TeacherRepository.GetAll().ToList().Select(Mapper.Map<TeacherDTO>).ToList();
        }
    }
}

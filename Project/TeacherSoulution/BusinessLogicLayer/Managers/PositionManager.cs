﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer.Interfaces;
using Entities.DTOModels;
using Entities.Models;

namespace BusinessLogicLayer.Managers
{
    public class PositionManager : IBaseManager<PositionDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidator _validator;

        public PositionManager(IUnitOfWork unitOfWork, IValidator validator)
        {
            _unitOfWork = unitOfWork;
            _validator = validator;
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
        }

        public void Create(PositionDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.PositionRepository.Create(Mapper.Map<Position>(item));
            _unitOfWork.Save();
        }

        public void Update(PositionDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.PositionRepository.Update(Mapper.Map<Position>(item));
            _unitOfWork.Save();
        }

        public void Delete(int id)
        {
            ValidateId(id);
            _unitOfWork.PositionRepository.Delete(id);
            _unitOfWork.Save();
        }

        public PositionDTO GetById(int id)
        {
            ValidateId(id);
            return Mapper.Map<PositionDTO>(_unitOfWork.PositionRepository.GetById(id));
        }

        public IEnumerable<PositionDTO> GetAll()
        {
            return _unitOfWork.PositionRepository.GetAll().ToList().Select(Mapper.Map<PositionDTO>).ToList();
        }
    }
}

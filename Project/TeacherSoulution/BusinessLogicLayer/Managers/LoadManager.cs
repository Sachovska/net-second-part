﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer.Interfaces;
using Entities.DTOModels;
using Entities.Models;

namespace BusinessLogicLayer.Managers
{
    public class LoadManager : IBaseManager<LoadDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidator _validator;

        public LoadManager(IUnitOfWork unitOfWork, IValidator validator)
        {
            _unitOfWork = unitOfWork;
            _validator = validator;
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
        }

        public void Create(LoadDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.LoadRepository.Create(Mapper.Map<Load>(item));
            _unitOfWork.Save();
        }

        public void Update(LoadDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.LoadRepository.Update(Mapper.Map<Load>(item));
            _unitOfWork.Save();
        }

        public void Delete(int id)
        {
            ValidateId(id);
            _unitOfWork.LoadRepository.Delete(id);
            _unitOfWork.Save();
        }

        public LoadDTO GetById(int id)
        {
            ValidateId(id);
            return Mapper.Map<LoadDTO>(_unitOfWork.LoadRepository.GetById(id));
        }

        public IEnumerable<LoadDTO> GetAll()
        {
            return _unitOfWork.LoadRepository.GetAll().ToList().Select(Mapper.Map<LoadDTO>).ToList();
        }
    }
}

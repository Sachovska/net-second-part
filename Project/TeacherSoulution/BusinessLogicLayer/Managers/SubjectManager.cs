﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BusinessLogicLayer.Interfaces;
using DataAccessLayer.Interfaces;
using Entities.DTOModels;
using Entities.Models;

namespace BusinessLogicLayer.Managers
{
    public class SubjectManager : IBaseManager<SubjectDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IValidator _validator;

        public SubjectManager(IUnitOfWork unitOfWork, IValidator validator)
        {
            _unitOfWork = unitOfWork;
            _validator = validator;
        }

        private void ValidateId(int id)
        {
            if (id < 1)
                throw new ArgumentException("Id must be positive number");
        }

        public void Create(SubjectDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.SubjectRepository.Create(Mapper.Map<Subject>(item));
            _unitOfWork.Save();
        }

        public void Update(SubjectDTO item)
        {
            _validator.Validate(item);
            _unitOfWork.SubjectRepository.Update(Mapper.Map<Subject>(item));
            _unitOfWork.Save();
        }

        public void Delete(int id)
        {
            _unitOfWork.SubjectRepository.Delete(id);
            _unitOfWork.Save();
        }

        public SubjectDTO GetById(int id)
        {
            ValidateId(id);
            return Mapper.Map<SubjectDTO>(_unitOfWork.SubjectRepository.GetById(id));
        }

        public IEnumerable<SubjectDTO> GetAll()
        {
            return _unitOfWork.SubjectRepository.GetAll().ToList().Select(Mapper.Map<SubjectDTO>).ToList();
        }
    }
}

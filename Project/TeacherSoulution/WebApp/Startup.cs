﻿using BusinessLogicLayer;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Managers;
using DataAccessLayer;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Repositories;
using Entities.DTOModels;
using Entities.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TeachersDBContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IValidator, AttributeValidator>();

            services.AddTransient<IBaseRepository<Position>, PositionReprository>();
            services.AddTransient<IBaseRepository<Subject>, SubjectRepository>();
            services.AddTransient<IBaseRepository<Teacher>, TeacherRepository>();
            services.AddTransient<IBaseRepository<Load>, LoadRepository>();

            services.AddTransient<IBaseManager<SubjectDTO>, SubjectManager>();
            services.AddTransient<IBaseManager<PositionDTO>, PositionManager>();
            services.AddTransient<IBaseManager<TeacherDTO>, TeacherManager>();
            services.AddTransient<IBaseManager<LoadDTO>, LoadManager>();
            
            Mapping.Configure();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

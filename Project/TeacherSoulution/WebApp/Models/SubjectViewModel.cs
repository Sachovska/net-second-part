﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class SubjectViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(120, ErrorMessage = "The name should have length till 50 charactares")]
        public string Name { get; set; }

        [Required]
        public int Hour { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class TeacherViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The surname should have length till 50 charactares")]
        public string Surname { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The name should have length till 50 charactares")]
        public string Name { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The middle name should have length till 50 charactares")]
        public string MiddleName { get; set; }

        [Required]
        public string Position { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "The mobile should have length till 10 charactares")]
        public string Mobile { get; set; }

        [Required]
        [StringLength(120, ErrorMessage = "The address should have length till 120 charactares")]
        public string Address { get; set; }
    }
}

﻿using AutoMapper;
using Entities.DTOModels;
using Entities.Models;

namespace WebApp
{
    public class Mapping : Profile
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<PositionDTO, Position>();
                cfg.CreateMap<Position, PositionDTO>()
                    .ForMember(x => x.Id, y => y.MapFrom(t => t.Id))
                    .ForMember(x => x.Name, y => y.MapFrom(t => t.Name))
                    .ForMember(x => x.RateHour, y => y.MapFrom(t => t.RateHour));

                cfg.CreateMap<SubjectDTO, Subject>();
                cfg.CreateMap<Subject, SubjectDTO>()
                    .ForMember(x => x.Id, y => y.MapFrom(t => t.Id))
                    .ForMember(x => x.Name, y => y.MapFrom(t => t.Name));

                cfg.CreateMap<TeacherDTO, Teacher>();
                cfg.CreateMap<Teacher, TeacherDTO>()
                    .ForMember(x => x.Id, y => y.MapFrom(t => t.Id))
                    .ForMember(x => x.Surname, y => y.MapFrom(t => t.Surname))
                    .ForMember(x => x.Name, y => y.MapFrom(t => t.Name))
                    .ForMember(x => x.MiddleName, y => y.MapFrom(t => t.MiddleName))
                    .ForMember(x => x.IdPosition, y => y.MapFrom(t => t.IdPosition))
                    .ForMember(x => x.Mobile, y => y.MapFrom(t => t.Mobile))
                    .ForMember(x => x.Address, y => y.MapFrom(t => t.Address));

                cfg.CreateMap<LoadDTO, Load>();
                cfg.CreateMap<Load, LoadDTO>()
                    .ForMember(x => x.Id, y => y.MapFrom(t => t.Id))
                    .ForMember(x => x.IdTeacher, y => y.MapFrom(t => t.IdTeacher))
                    .ForMember(x => x.IdSubject, y => y.MapFrom(t => t.IdSubject))
                    .ForMember(x => x.Hour, y => y.MapFrom(t => t.Hour));
            });
        }
    }
}

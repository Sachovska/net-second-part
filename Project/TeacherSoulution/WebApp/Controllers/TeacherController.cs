﻿using System.Collections.Generic;
using System.Linq;
using BusinessLogicLayer.Interfaces;
using Entities.DTOModels;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class TeacherController : Controller
    {
        private readonly IBaseManager<TeacherDTO> _teacherManager;
        private readonly IBaseManager<PositionDTO> _positionManager;
        private readonly IBaseManager<LoadDTO> _loadManager;
        private readonly IBaseManager<SubjectDTO> _subjectManager;

        public TeacherController(IBaseManager<TeacherDTO> teacherManager, IBaseManager<PositionDTO> positionManager,
            IBaseManager<LoadDTO> loadManager, IBaseManager<SubjectDTO> subjectManager)
        {
            _teacherManager = teacherManager;
            _positionManager = positionManager;
            _loadManager = loadManager;
            _subjectManager = subjectManager;
        }

        // GET: Teacher
        public ActionResult Index()
        {
            var teachers = new List<TeacherViewModel>();
            foreach (var teacher in _teacherManager.GetAll())
            {
                var position = _positionManager.GetById(teacher.IdPosition).Name;
                var teacherViewModel = new TeacherViewModel
                {
                    Id = teacher.Id,
                    Name = teacher.Name,
                    Surname = teacher.Surname,
                    MiddleName = teacher.MiddleName,
                    Address = teacher.Address,
                    Mobile = teacher.Mobile,
                    Position = position
                };
                teachers.Add(teacherViewModel);
            }
            return View(teachers);
        }

        // GET: Teacher/Details/5
        public ActionResult Details(int id)
        {
            var teacher = _teacherManager.GetById(id);
            var loadOfTeacher = _loadManager.GetAll().Where(l => l.IdTeacher.Equals(teacher.Id)).Distinct().ToList();
            var subjects = new List<SubjectViewModel>();
            foreach (var load in loadOfTeacher)
            {
                var subject = _subjectManager.GetById(load.IdSubject).Name;
                var hour = load.Hour;
                subjects.Add(new SubjectViewModel {Id = load.Id, Name = subject, Hour = hour});
            }
            ViewBag.IdTeacher = id;
            return View(subjects);
        }

        // GET: Teacher/Create
        public ActionResult Create()
        {
            ViewBag.Positions = _positionManager.GetAll();
            return View();
        }

        // POST: Teacher/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TeacherViewModel teacherViewModel)
        {
            if (ModelState.IsValid)
            {
                var teacher = new TeacherDTO
                {
                    Surname = teacherViewModel.Surname,
                    Name = teacherViewModel.Name,
                    MiddleName = teacherViewModel.MiddleName,
                    IdPosition = _positionManager.GetAll().Where(t => t.Name.Equals(teacherViewModel.Position)).ToList().First().Id,
                    Address = teacherViewModel.Address,
                    Mobile = teacherViewModel.Mobile
                };
                _teacherManager.Create(teacher);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        // GET: Teacher/Edit/5
        public ActionResult Edit(int id)
        {
            var teacher = _teacherManager.GetById(id);
            var teacherViewModel = new TeacherViewModel
            {
                Id = teacher.Id,
                Surname = teacher.Surname,
                Name = teacher.Name,
                MiddleName = teacher.MiddleName,
                Position = _positionManager.GetById(teacher.IdPosition).Name,
                Address = teacher.Address,
                Mobile = teacher.Mobile
            };
            ViewBag.Positions = _positionManager.GetAll();
            return View(teacherViewModel);
        }

        // POST: Teacher/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, TeacherViewModel teacherViewModel)
        {
            if (ModelState.IsValid)
            {
                var idPosition = _positionManager.GetAll().Where(t => t.Name.Equals(teacherViewModel.Position)).ToList()
                    .First().Id;
                var teacher = new TeacherDTO
                {
                    Id = teacherViewModel.Id,
                    Surname = teacherViewModel.Surname,
                    Name = teacherViewModel.Name,
                    MiddleName = teacherViewModel.MiddleName,
                    IdPosition = idPosition,
                    Address = teacherViewModel.Address,
                    Mobile = teacherViewModel.Mobile
                };
                _teacherManager.Update(teacher);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        // GET: Teacher/Delete/5
        public ActionResult Delete(int id)
        {
            var teacher = _teacherManager.GetById(id);
            return View(teacher);
        }

        // POST: Teacher/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, TeacherDTO teacher)
        {
            try
            {
                _teacherManager.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult AddSubject()
        {
            ViewBag.Subjects = _subjectManager.GetAll();
            return View();
        }

        // POST: Teacher/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSubject(int id, SubjectViewModel subjectViewModel)
        {

            if (ModelState.IsValid)
            {
                var load = new LoadDTO
                {
                    IdTeacher = id,
                    IdSubject = _subjectManager.GetAll().Where(s => s.Name.Equals(subjectViewModel.Name))
                        .Select(t => t.Id).FirstOrDefault(),
                    Hour = subjectViewModel.Hour
                };
                _loadManager.Create(load);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        // GET: Teacher/Delete/5
        public ActionResult DeleteSubject(int id)
        {
            var load = _loadManager.GetById(id);
            var subject = _subjectManager.GetById(load.IdSubject);
            var subjectViewModel = new SubjectViewModel {Id = load.Id, Name = subject.Name, Hour = load.Hour};
            return View(subjectViewModel);
        }

        // POST: Teacher/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteSubject(int id, SubjectViewModel subjectViewModel)
        {
            try
            {
                _loadManager.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
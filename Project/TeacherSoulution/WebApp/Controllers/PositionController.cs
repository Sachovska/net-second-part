﻿using BusinessLogicLayer.Interfaces;
using Entities.DTOModels;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class PositionController : Controller
    {
        private readonly IBaseManager<PositionDTO>_positionManager;

        public PositionController(IBaseManager<PositionDTO> positionManager)
        {
            _positionManager = positionManager;
        }

        // GET: Position
        public ViewResult Index()
        {
            return View(_positionManager.GetAll());
        }
        
        // GET: Position/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Position/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PositionDTO position)
        {
            if (ModelState.IsValid)
            {
                _positionManager.Create(position);
                return RedirectToAction(nameof(Index));
            }

            return View();
        }

        // GET: Position/Edit/5
        public ActionResult Edit(int id)
        {
            var position = _positionManager.GetById(id);
            return View(position);
        }

        // POST: Position/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, PositionDTO position)
        {
            if (ModelState.IsValid)
            {
                _positionManager.Update(position);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        // GET: Position/Delete/5
        public ActionResult Delete(int id)
        {
            var position = _positionManager.GetById(id);
            return View(position);
        }

        // POST: Position/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, Position position)
        {
            try
            {
                _positionManager.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
﻿using BusinessLogicLayer.Interfaces;
using Entities.DTOModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class SubjectController : Controller
    {
        private readonly IBaseManager<SubjectDTO> _subjectManager;

        public SubjectController(IBaseManager<SubjectDTO> subjectManager)
        {
            _subjectManager = subjectManager;
        }

        // GET: Subject
        public ActionResult Index()
        {
            return View(_subjectManager.GetAll());
        }
        
        // GET: Subject/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Subject/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SubjectDTO subject)
        {
            if (ModelState.IsValid)
            {
                _subjectManager.Create(subject);
                return RedirectToAction(nameof(Index));
            }

            return View();
        }

        // GET: Subject/Edit/5
        public ActionResult Edit(int id)
        {
            var subject = _subjectManager.GetById(id);
            return View(subject);
        }

        // POST: Subject/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, SubjectDTO subject)
        {
            if (ModelState.IsValid)
            {
                _subjectManager.Update(subject);
                return RedirectToAction(nameof(Index));
            }

            return View();
        }

        // GET: Subject/Delete/5
        public ActionResult Delete(int id)
        {
            var subject = _subjectManager.GetById(id);
            return View(subject);
        }

        // POST: Subject/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                _subjectManager.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
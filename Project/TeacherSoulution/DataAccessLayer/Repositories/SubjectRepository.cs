﻿using System.Collections.Generic;
using DataAccessLayer.Interfaces;
using Entities.Models;

namespace DataAccessLayer.Repositories
{
    public class SubjectRepository:IBaseRepository<Subject>
    {
        private readonly TeachersDBContext _dbContext;

        public SubjectRepository(TeachersDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Subject item)
        {
            _dbContext.Subject.Add(item);
        }

        public void Update(Subject item)
        {
            _dbContext.Subject.Update(item);
        }

        public void Delete(int id)
        {
            var group = _dbContext.Subject.Find(id);
            if(group != null) _dbContext.Subject.Remove(group);
        } 

        public Subject GetById(int id)
        {
            return _dbContext.Subject.Find(id);
        }

        public IEnumerable<Subject> GetAll()
        {
            return _dbContext.Subject;
        }
    }
}

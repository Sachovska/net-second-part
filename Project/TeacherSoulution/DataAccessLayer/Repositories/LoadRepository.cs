﻿using System.Collections.Generic;
using DataAccessLayer.Interfaces;
using Entities.Models;

namespace DataAccessLayer.Repositories
{
    public class LoadRepository : IBaseRepository<Load>
    {
        private readonly TeachersDBContext _dbContext;

        public LoadRepository(TeachersDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Load item)
        {
            _dbContext.Load.Add(item);
        }

        public void Update(Load item)
        {
            _dbContext.Load.Update(item);
        }

        public void Delete(int id)
        {
            var load = _dbContext.Load.Find(id);
            if (load != null) _dbContext.Load.Remove(load);
        }

        public Load GetById(int id)
        {
            return _dbContext.Load.Find(id);
        }

        public IEnumerable<Load> GetAll()
        {
            return _dbContext.Load;
        }
    }
}


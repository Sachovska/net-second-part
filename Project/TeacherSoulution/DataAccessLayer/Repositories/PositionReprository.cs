﻿using System.Collections.Generic;
using DataAccessLayer.Interfaces;
using Entities.Models;

namespace DataAccessLayer.Repositories
{
    public class PositionReprository:IBaseRepository<Position>
    {
        private readonly TeachersDBContext _dbContext;

        public PositionReprository(TeachersDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Position item)
        {
            _dbContext.Position.Add(item);
        }

        public void Update(Position item)
        {
            _dbContext.Position.Update(item);
        }

        public void Delete(int id)
        {
            var position = _dbContext.Position.Find(id);
            if (position != null) _dbContext.Position.Remove(position);
        }

        public Position GetById(int id)
        {
            return _dbContext.Position.Find(id);
        }

        public IEnumerable<Position> GetAll()
        {
            return _dbContext.Position;
        }
    }
}

﻿using System.Collections.Generic;
using DataAccessLayer.Interfaces;
using Entities.Models;

namespace DataAccessLayer.Repositories
{
    public class TeacherRepository:IBaseRepository<Teacher>
    {
        private readonly TeachersDBContext _dbContext;

        public TeacherRepository(TeachersDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Teacher item)
        {
            _dbContext.Teacher.Add(item);
        }

        public void Update(Teacher item)
        {
            _dbContext.Teacher.Update(item);
        }

        public void Delete(int id)
        {
            var teacher = _dbContext.Teacher.Find(id);
            if (teacher != null) _dbContext.Teacher.Remove(teacher);
        }

        public Teacher GetById(int id)
        {
            return _dbContext.Teacher.Find(id);
        }

        public IEnumerable<Teacher> GetAll()
        {
            return _dbContext.Teacher;
        }
    }
}

﻿using Entities.Models;

namespace DataAccessLayer.Interfaces
{
    public interface IUnitOfWork
    {
        IBaseRepository<Position> PositionRepository { get; }
        IBaseRepository<Subject> SubjectRepository { get; }
        IBaseRepository<Teacher> TeacherRepository { get; }
        IBaseRepository<Load> LoadRepository { get; }

        void Dispose();
        int Save();
    }
}

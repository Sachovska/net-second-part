﻿using System;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Repositories;
using Entities.Models;

namespace DataAccessLayer
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TeachersDBContext _dbContext;

        private IBaseRepository<Position> _positionRepository;
        private IBaseRepository<Subject> _subjectRepository;
        private IBaseRepository<Teacher> _teacherRepository;
        private IBaseRepository<Load> _loadRepository;

        public UnitOfWork()
        {
            _dbContext = new TeachersDBContext();
            _positionRepository = new PositionReprository(_dbContext);
            _subjectRepository = new SubjectRepository(_dbContext);
            _teacherRepository = new TeacherRepository(_dbContext);
            _loadRepository = new LoadRepository(_dbContext);
        }

        public IBaseRepository<Position> PositionRepository
        {
            get
            {
                if (_positionRepository != null) _positionRepository = new PositionReprository(_dbContext);
                return _positionRepository;
            }
        }

        public IBaseRepository<Subject> SubjectRepository
        {
            get
            {
                if (_subjectRepository != null) _subjectRepository = new SubjectRepository(_dbContext);
                return _subjectRepository;
            }
        }

        public IBaseRepository<Teacher> TeacherRepository
        {
            get
            {
                if (_teacherRepository != null) _teacherRepository = new TeacherRepository(_dbContext);
                return _teacherRepository;
            }
        }

        public IBaseRepository<Load> LoadRepository
        {
            get
            {
                if (_loadRepository != null) _loadRepository = new LoadRepository(_dbContext);
                return _loadRepository;
            }
        }


        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int Save()
        {
            return _dbContext.SaveChanges();
        }
    }
}
